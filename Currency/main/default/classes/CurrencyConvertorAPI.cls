public class CurrencyConvertorAPI {
    public static decimal convertor(String cur1,decimal d1,String cur2)
    {
        decimal result=0;
        HTTP h=new HTTP();
        HTTPRequest req=new HTTPRequest();
        req.setEndpoint('http://data.fixer.io/api/latest?access_key=9b3bdf1dbaef8ac288f259790ab438e7');
        req.setMethod('GET');
        HTTPResponse res=h.send(req);
        map<String,Object>jsonBody=(map<String,Object>)Json.deserializeUntyped(res.getBody());
      
        Map<String,Object>curMap=(Map<String,Object>)jsonBody.get('rates');
       
        List<Currency__c> cur=new List<Currency__c>();
         for (String fieldName : curMap.keySet()){

           // System.debug('field name is ' + fieldName+curMap.get(fieldName));
             decimal res1;
             
             res1=(Decimal)curMap.get(fieldName);

            Currency__c c=new Currency__c();
             c.Name=fieldName;
             c.ExchangeRate__c=res1;
             cur.add(c);
           
        }
        insert cur;
        List<Currency__c> fullList = [Select Name,ExchangeRate__c From Currency__c];
Set<String> orderObj = new Set<String>(); 
List<Currency__c> dupList = new List<Currency__c>();

for(Currency__c o : fullList)
{
    if(orderObj.contains(o.Name))
    {
        dupList.add(o);
    }
    else
    {
        orderObj.add(o.Name);
    }
}

delete dupList;
       Decimal conversionRate1=(Decimal)curMap.get(cur1);
        Decimal conversionRate2=(Decimal)curMap.get(cur2);
        result=(conversionRate2/conversionRate1)*d1;
        return result;
    }
}