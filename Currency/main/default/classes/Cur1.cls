public without sharing class Cur1 {
    public class FlowInputs {
        @InvocableVariable public String currName;
        @InvocableVariable public Decimal exchangeRate ;
    }

    @InvocableMethod
    public static void updateOpp (List<FlowInputs> request) {

        String currName = request[0].currName;

        Decimal exchangeRate= request[0].exchangeRate;
        Currency__c c=new Currency__c();
        c.Name=currName;
        c.ExchangeRate__c=exchangeRate;
insert c;
        System.debug(currName+exchangeRate); 

    } 

}